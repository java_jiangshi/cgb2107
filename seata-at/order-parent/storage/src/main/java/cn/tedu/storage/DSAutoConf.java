package cn.tedu.storage;

import com.zaxxer.hikari.HikariDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DSAutoConf {
    // 创建原始数据源
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource getDataSource() {
        return new HikariDataSource();
    }

    // 创建数据源代理
    @Primary  // 首选对象
    @Bean
    public DataSource getDataSourceProxy(DataSource ds) {
        return new DataSourceProxy(ds);
    }
}
