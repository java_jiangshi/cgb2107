package cn.tedu.order.service;
import cn.tedu.order.entity.Order;
import cn.tedu.order.feign.AccountClient;
import cn.tedu.order.feign.EasyIdClient;
import cn.tedu.order.feign.StorageClient;
import cn.tedu.order.mapper.OrderMapper;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private EasyIdClient easyIdClient;
    @Autowired
    private AccountClient accountClient;
    @Autowired
    private StorageClient storageClient;

    @Transactional // 控制本地事务
    @GlobalTransactional // 启动全局事务，只加载第一个模块
    @Override
    public void create(Order order) {
        // 远程调用id发号器，生产订单ID
        String s = easyIdClient.getId("order_business");
        Long orderId = Long.valueOf(s);

        order.setId(orderId);
        orderMapper.create(order);

        // 添加了 seata 依赖后，seata有自动配置，
        // 但是如果没有配置事务，项目无法启动

        // 远程调用库存，减少库存
        storageClient.decrease(order.getProductId(),order.getCount());
        // 远程调用账户，扣减金额
        accountClient.decrease(order.getUserId(),order.getMoney());
    }
}
