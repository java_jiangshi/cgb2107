package cn.tedu.es;

import cn.tedu.es.entity.Student;
import cn.tedu.es.repo.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

@SpringBootTest
public class Test1 {
    @Autowired
    private StudentRepository repo;

    @Test
    public void test1() {
        // 添加或修改数据
        repo.save(new Student(9527L, "唐伯虎", '男', "2021-11-04"));
        repo.save(new Student(9528L, "张伯虎", '男', "2020-02-25"));
        repo.save(new Student(9529L, "李伯虎", '女', "2021-10-18"));
        repo.save(new Student(9530L, "赵伯虎", '男', "2020-07-11"));
        repo.save(new Student(9531L, "华夫人", '女', "2020-03-24"));
        repo.save(new Student(9532L, "华安", '男', "2019-12-30"));
        repo.save(new Student(9533L, "旺财", '男', "2020-08-14"));
        repo.save(new Student(9534L, "祝枝山", '男', "2021-06-22"));
    }

    @Test
    public void test2() {
        repo.save(new Student(9532L, "如花", '女', "2019-05-03"));
    }

    @Test
    public void test3() {
        /*
        Optional 是 jdk 提供的一个数据包装对象，
        可以辅助防止出现空指针异常
         */
        Optional<Student> op = repo.findById(9527L);
        if (op.isPresent()) { //内部的学生对象是否存在
            System.out.println(op.get());
        }
        System.out.println("----------------------------------");
        Iterable<Student> all = repo.findAll();
        for (Student s : all) {
            System.out.println(s);
        }
    }

    @Test
    public void test4() {
        repo.deleteById(9533L);
    }

    @Test
    public void test5() {
        List<Student> list1 = repo.findByName("伯虎");
        List<Student> list2 = repo.findByName("华");

        System.out.println(list1);
        System.out.println(list2);
    }

    @Test
    public void test6() {
        /*
        Pageable 对象，封装分页参数：
            - 页号，从0开始
            - 每页大小
         */
        Pageable pageable = PageRequest.of(2, 2);

        List<Student> list = repo.findByNameOrBirthDate("伯虎", "2021-06-22",pageable);
        for (Student s : list) {
            System.out.println(s);
        }
    }
}