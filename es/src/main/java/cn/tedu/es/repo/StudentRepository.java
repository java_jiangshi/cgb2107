package cn.tedu.es.repo;

import cn.tedu.es.entity.Student;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/*
Spring Data API 定义的 Repository 规范，
只需要定义接口，就可以实现数据增删改查
 */
public interface StudentRepository
        extends ElasticsearchRepository<Student, Long> { //数据的类型, ID的类型
    // 在姓名字段中搜索关键词
    List<Student> findByName(String key);

    // 在姓名字段中搜索，在出生日期中搜索
    // import org.springframework.data.domain.Pageable;
    List<Student> findByNameOrBirthDate(
            String key, String birthDate, Pageable pageable);
}
