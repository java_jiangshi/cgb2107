package cn.tedu.es.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

/*
spring es api 可以根据这里的设置，在服务器上新建索引，
如果索引已经存在，不会重复创建

索引一般手动创建，不应该依赖api自动创建
 */
@Document(indexName = "students",shards = 3,replicas = 2)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    @Id // 使用学生的学号作为索引id（_id）
    private Long id;
    private String name;
    private Character gender;
    @Field("birthDate") //es服务器中的字段名，与属性名相同可以省略
    private String birthDate;
}
