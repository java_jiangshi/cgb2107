package com.pd.es;
import com.pd.pojo.Item;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Highlight;
import org.springframework.data.elasticsearch.annotations.HighlightField;
import org.springframework.data.elasticsearch.annotations.HighlightParameters;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface ItemRepository
    extends ElasticsearchRepository<Item, Long> {

    @Highlight(
            parameters = @HighlightParameters(
                    preTags = "<em>",
                    postTags = "</em>"
            ),
            fields = @HighlightField(name = "title")
    )
    List<SearchHit<Item>>  findByTitleOrSellPoint(
            String key1, String key2, Pageable pageable);

}
