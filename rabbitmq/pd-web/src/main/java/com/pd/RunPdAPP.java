package com.pd;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@MapperScan("com.pd.mapper")
public class RunPdAPP{
	
	public static void main(String[] args) {
		SpringApplication.run(RunPdAPP.class, args);
	}

	// 新建 spring 的 Queue 实例用来封装队列的参数
	// rabbitmq的自动配置类会自动发现这个Queue实例
	// 根据其中的参数自动在服务器上创建队列
	// import org.springframework.amqp.core.Queue;
	@Bean
	public Queue orderQueue() {
		// 持久， 非独占， 不自动删除
		return new Queue("orderQueue",true,false,false);
	}


}
