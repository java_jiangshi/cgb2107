package com.pd.service;

import com.pd.pojo.Item;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchHit;

import java.util.List;

public interface SearchService {
    List<SearchHit<Item>> search(String key, Pageable pageable);
}
