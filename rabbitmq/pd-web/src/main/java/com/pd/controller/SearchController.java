package com.pd.controller;
import com.pd.pojo.Item;
import com.pd.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class SearchController {
    @Autowired
    private SearchService searchService;

    @GetMapping("/search/toSearch.html")  //   ?key=手机&page=3&size=20
    public String search(
            String key, Pageable pageable, Model model) {
        List<SearchHit<Item>> searchHits = searchService.search(key, pageable);
        /*
        从 SearchHit 取出高亮的title,放入原始的 Item对象
        高亮的title:
            "这是一个",
            "<em>手机</em>",
            "商品的标题"
            ----> "这是一个<em>手机</em>商品的标题"
         */

        // 创建 List<Item>，把 searchHits 集合的结果，处理成List<Item>集合
        List<Item> list = new ArrayList<>();
        for (SearchHit<Item> sh : searchHits) {
            List<String> hlTitle = sh.getHighlightField("title");
            Item item = sh.getContent();
            item.setTitle(pinJie(hlTitle)); //高亮title拼接后，放入item对象
            list.add(item);
        }

        model.addAttribute("list", list);
        model.addAttribute("page", pageable);
        return "/search.jsp";
    }

    private String pinJie(List<String> hlTitle) {
        StringBuilder sb = new StringBuilder();
        for (String s : hlTitle) {
            sb.append(s);
        }
        return sb.toString();
    }
}
