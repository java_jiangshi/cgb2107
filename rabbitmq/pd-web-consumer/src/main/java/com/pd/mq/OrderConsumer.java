package com.pd.mq;

import com.pd.pojo.PdOrder;
import com.pd.service.OrderService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
自动创建消费者实例，自动注册成为消费者，自动开始接收消息，
收到消息后自动执行消息处理方法存储订单
 */

// 通过注解配置就可以接收消息，不需要写代码
@RabbitListener(queues = "orderQueue")
@Component
public class OrderConsumer {
    @Autowired
    private OrderService orderService;

    //@RabbitHandler 配合 @RabbitListener
    //来指定处理消息的方法
    //一个类中，@RabbitHandler注解只能出现一次
    @RabbitHandler
    public void receive(PdOrder order) throws Exception {
        orderService.saveOrder(order);
        System.out.println("------------------------------订单已存储");
    }
}
