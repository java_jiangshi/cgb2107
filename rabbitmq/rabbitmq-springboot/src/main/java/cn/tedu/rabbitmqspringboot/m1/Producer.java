package cn.tedu.rabbitmqspringboot.m1;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    // 发送消息的封装工具
    // RabbitAutoConfiguration 中创建了 AmqpTemplate 实例
    @Autowired
    private AmqpTemplate t;

    public void send() {
        // 向 helloworld 队列发送消息
        // 队列的参数在启动类中设置
        t.convertAndSend("helloworld", "Hello world!");
    }
}
