package cn.tedu.rabbitmqspringboot.m2;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    // 发送消息的封装工具
    // RabbitAutoConfiguration 中创建了 AmqpTemplate 实例
    @Autowired
    private AmqpTemplate t;

    public void send(String s) {
        // 向 helloworld 队列发送消息
        // 队列的参数在启动类中设置
        t.convertAndSend("task_queue", s);

        /*
        t.convertAndSend("task_queue", s, 消息预处理对象);
        在预处理对象中，可以对消息的参数进行调整，
        可以把持久化参数设置成非持久
         */
    }
}
