package cn.tedu.rabbitmqspringboot.m1;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Main1 {

    public static void main(String[] args) {
        SpringApplication.run(Main1.class, args);
    }

    // 设置队列参数
    @Bean
    public Queue helloworldQueue() {
        // 非持久，非独占，不自动删除
        return new Queue("helloworld",false,false,false);
    }

    @Autowired
    private Producer p;
    /*
    spring的主线程执行流程
    自动扫描创建实例 --> 完成依赖注入 --> @PostConstruct --> 后续步骤
     */
    @PostConstruct
    public void test() {
        // 在新线程中执行自己的运算，不阻塞 spring 主线程执行
        new Thread(() -> {
            try {
                Thread.sleep(3000L); //等待helloworld队列被创建出来
            } catch (InterruptedException e) {
            }
            p.send();
        }).start();
    }

}
