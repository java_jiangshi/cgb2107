package cn.tedu.rabbitmqspringboot.m4;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import java.util.Scanner;

@SpringBootApplication
public class Main4 {

    public static void main(String[] args) {
        SpringApplication.run(Main4.class, args);
    }

    @Bean
    public DirectExchange logs() {
        // 非持久，不自动删除
        return new DirectExchange("direct_logs", false, false);
    }

    @Autowired
    private Producer p;
    /*
    spring的主线程执行流程
    自动扫描创建实例 --> 完成依赖注入 --> @PostConstruct --> 后续步骤
     */
    @PostConstruct
    public void test() {
        // 在新线程中执行自己的运算，不阻塞 spring 主线程执行
        new Thread(() -> {
            while (true){
                System.out.print("输入消息：");
                String s = new Scanner(System.in).nextLine();
                System.out.print("输入路由键：");
                String k = new Scanner(System.in).nextLine();
                p.send(k, s);
            }
        }).start();
    }

}
