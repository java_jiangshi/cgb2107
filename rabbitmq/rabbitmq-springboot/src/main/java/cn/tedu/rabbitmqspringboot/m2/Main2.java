package cn.tedu.rabbitmqspringboot.m2;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import java.util.Scanner;

/*
    合理分发
          1. autoAck=false，手动确认
             spring封装的rabbitmq，默认就是手动确认，
             spring会自动执行发送回执

          2. qos=1，预抓取消息数量
             yml 添加 pre-fetch=1，默认是250

    消息持久化
          1. 队列持久化
             new Queue("", true)

          2. 消息数据的持久化
             spring发送的消息，默认就是持久消息
 */

@SpringBootApplication
public class Main2 {

    public static void main(String[] args) {
        SpringApplication.run(Main2.class, args);
    }

    // 设置队列参数
    @Bean
    public Queue taskQueue() {
        // 持久，非独占，不自动删除
        return new Queue("task_queue"); //只给队列名，其他参数是下面的默认值
        // return new Queue("task_queue",true,false,false);
    }

    @Autowired
    private Producer p;
    /*
    spring的主线程执行流程
    自动扫描创建实例 --> 完成依赖注入 --> @PostConstruct --> 后续步骤
     */
    @PostConstruct
    public void test() {
        // 在新线程中执行自己的运算，不阻塞 spring 主线程执行
        new Thread(() -> {
            while (true){
                System.out.print("输入消息：");
                String s = new Scanner(System.in).nextLine();
                p.send(s);
            }
        }).start();
    }

}
