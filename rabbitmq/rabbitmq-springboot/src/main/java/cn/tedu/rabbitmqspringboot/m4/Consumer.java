package cn.tedu.rabbitmqspringboot.m4;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

//每个@RabbitListener都会注册成为一个消费者
@Component
public class Consumer {
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue,   //队列,随机命名,false,true,true
            // declare = "false" 不创建交换机，而是使用已存在的交换机
            exchange = @Exchange(name = "direct_logs", declare = "false"), //交换机
            key = {"error"}
    ))
    public void receive1(String msg) {
        System.out.println("消费者1收到："+msg);
    }

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue,
            exchange = @Exchange(name = "direct_logs", declare = "false"), //交换机
            key = {"info","error","warning"}
    ))
    public void receive2(String msg) {
        System.out.println("消费者2收到："+msg);
    }
}
