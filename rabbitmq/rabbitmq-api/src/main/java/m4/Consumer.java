package m4;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140"); // wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        f.setVirtualHost("/"); //默认空间，可以在服务器创建自己的空间使用
        Channel c = f.newConnection().createChannel();

        // 1.随机队列 2.交换机 3.绑定，设置绑定键
        // 让服务器自动命名，自动提供队列参数: 随机名,false,true,true
        String queue = c.queueDeclare().getQueue();
        c.exchangeDeclare("direct_logs", BuiltinExchangeType.DIRECT);
        System.out.println("输入绑定键，用空格隔开："); // "aa  bb   cc"
        String s = new Scanner(System.in).nextLine();
        String[] a = s.split("\\s+");//  \s是空白字符， +表示一到多个
        for (String k : a) {
            c.queueBind(queue, "direct_logs", k);
        }

        // 从随机队列接收处理消息
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            String msg = new String(message.getBody());
            System.out.println("收到： "+msg);
        };
        CancelCallback cancelCallback = consumerTag -> {};
        c.basicConsume(queue, true, deliverCallback, cancelCallback);
    }
}
