package m4;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140"); // wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        f.setVirtualHost("/"); //默认空间，可以在服务器创建自己的空间使用
        Channel c = f.newConnection().createChannel();

        // 创建 Direct 交换机： direct_logs
        c.exchangeDeclare("direct_logs", BuiltinExchangeType.DIRECT);
        // 发送消息，在消息上携带路由键
        while (true) {
            System.out.print("输入消息：");
            String s = new Scanner(System.in).nextLine();
            System.out.print("输入路由键：");
            String k = new Scanner(System.in).nextLine();
            // 第二个参数：路由键关键词
            // 如果使用默认交换机，路由键就是队列名
            c.basicPublish("direct_logs", k, null,s.getBytes());
        }
    }
}

