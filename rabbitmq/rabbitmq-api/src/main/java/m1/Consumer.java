package m1;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");   // wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Connection con = f.newConnection();
        Channel c = con.createChannel();// 通信的通道
        // 在服务器上创建 helloworld 队列
        c.queueDeclare("helloworld",false,false,false,null);

        // 创建回调对象
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            byte[] a = message.getBody();
            String s = new String(a);
            System.out.println("收到："+s);
        };
        CancelCallback cancelCallback = consumerTag -> {};

        // 从 helloworld 队列接收消息，把消息传递到回调对象处理
        // c.basicConsume("helloworld", true, 处理消息的回调对象, 取消消息处理的回调对象);
        /*
        第二个参数： 确认方式 ACK -- Acknowledgment
           - true  自动确认
           - false 手动确认
         */
        c.basicConsume("helloworld", true, deliverCallback, cancelCallback);
    }
}
