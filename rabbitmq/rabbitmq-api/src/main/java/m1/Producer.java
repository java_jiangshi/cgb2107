package m1;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");   // wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Connection con = f.newConnection();
        Channel c = con.createChannel();// 通信的通道

        // 在服务器上创建 helloworld 队列
        // 队列如果已经存在，不会重复创建
        /*
        参数：
            1. 队列名
            2. 是否是持久队列
            3. 是否是排他队列（独占队列）
            4. 是否是自动删除的队列（没有消费者时，服务器是否自动删除）
            5. 队列的其他参数属性
         */
        c.queueDeclare("helloworld",false,false,false,null);

        // 向 helloworld 队列发送消息
        // 第1个参数： 交换机，空串是默认交换机
        // 第3个参数： 消息的其他参数属性
        c.basicPublish("", "helloworld", null, "Hello world!".getBytes());


    }
}
