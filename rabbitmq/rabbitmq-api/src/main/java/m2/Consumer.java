package m2;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static void main(String[] args) throws IOException, TimeoutException {
        // 连接
        ConnectionFactory f = new ConnectionFactory();
        f.setHost("192.168.64.140");   // wht6.cn
        f.setPort(5672);
        f.setUsername("admin");
        f.setPassword("admin");
        Connection con = f.newConnection();
        Channel c = con.createChannel();// 通信的通道

        c.queueDeclare("task_queue",true,false,false,null);

        // 回调对象
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            String s = new String(message.getBody());
            System.out.println(s);
            // 遍历所有字符，遇到 '.' 暂停1秒
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '.') {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                }
            }
            // c.basicAck(回执, 是否同时确认收到过的所有消息);
            c.basicAck(message.getEnvelope().getDeliveryTag(), false);
            System.out.println("-------------------------------消息处理完成");
        };
        CancelCallback cancelCallback = consumerTag -> {};
        // 每次只收一条，处理完之前不收下一条
        c.basicQos(1);

        // 接收消息
        // 第二个参数用 false, 手动确认，手动发送回执
        c.basicConsume("task_queue",false,deliverCallback,cancelCallback);
    }
}
