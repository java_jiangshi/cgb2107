package cn.tedu.order.service;
import cn.tedu.order.entity.AccountMessage;
import cn.tedu.order.entity.Order;
import cn.tedu.order.entity.TxInfo;
import cn.tedu.order.feign.AccountClient;
import cn.tedu.order.feign.EasyIdClient;
import cn.tedu.order.feign.StorageClient;
import cn.tedu.order.mapper.OrderMapper;
import cn.tedu.order.mapper.TxMapper;
import cn.tedu.order.util.JsonUtil;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;
import java.util.UUID;

@RocketMQTransactionListener
@Service
public class OrderServiceImpl
        implements OrderService, RocketMQLocalTransactionListener {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private EasyIdClient easyIdClient;
    @Autowired
    private AccountClient accountClient;
    @Autowired
    private StorageClient storageClient;
    // 封装工具，封装了发送消息的操作
    @Autowired
    private RocketMQTemplate t;
    @Autowired
    private TxMapper txMapper;

    @Override
    public void create(Order order) {
        // 产生一个事务id
        String xid = UUID.randomUUID().toString().replace("-", "");
        // 用 AccountMessage 封装调用信息
        AccountMessage am = new AccountMessage(
                order.getUserId(), order.getMoney(), xid);
        // 再转成 json 字符串
        String json = JsonUtil.to(am);
        // json字符串封装到spring的通用 Message 对象
        Message<String> msg =
            MessageBuilder
            .withPayload(json) //json字符串会自动转成byte[]数组
            .build();

        // 在业务方法中，不直接直接业务，
        // 而是发送事务消息，触发监听器执行本地事务
        // t.sendMessageInTransaction("orderTopic", msg, 业务数据参数);
        t.sendMessageInTransaction("orderTopic", msg, order);
    }

    // 执行业务运算的方法
    public void doCreate(Order order) {
        // 远程调用id发号器，生产订单ID
        String s = easyIdClient.getId("order_business");
        Long orderId = Long.valueOf(s);

        order.setId(orderId);
        orderMapper.create(order);

        /*
        不再使用 feign 做远程调用，而是要使用消息服务做异步调用
         */
        // 远程调用库存，减少库存
        //storageClient.decrease(order.getProductId(),order.getCount());
        // 远程调用账户，扣减金额
        //accountClient.decrease(order.getUserId(),order.getMoney());
    }
    // 通过发送事务消息，触发执行本地事务的方法
    @Transactional
    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(
            Message message, Object o) {
        RocketMQLocalTransactionState state; //用来返回
        int status; //用来存储到数据库表

        try {
            Order order = (Order) o;
            doCreate(order);
            state = RocketMQLocalTransactionState.COMMIT;
            status = 0;
        } catch (Exception e) {
            e.printStackTrace();
            state = RocketMQLocalTransactionState.ROLLBACK;
            status = 1;
        }
        // msg - AccountMessage {userId:1, money:100, xid:t34t345}
        // 从消息对象获取 xid
        String json = new String((byte[]) message.getPayload());
        String xid = JsonUtil.getString(json, "xid");

        txMapper.insert(new TxInfo(xid, status, System.currentTimeMillis()));
        return state;
    }

    // 处理 Rocketmq 事务状态回查
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        String json = new String((byte[]) message.getPayload());
        String xid = JsonUtil.getString(json, "xid");

        TxInfo txInfo = txMapper.selectById(xid);
        if (txInfo == null) {
            return RocketMQLocalTransactionState.UNKNOWN;
        }
        switch (txInfo.getStatus()) {
            case 0: return RocketMQLocalTransactionState.COMMIT;
            case 1: return RocketMQLocalTransactionState.ROLLBACK;
            default:return RocketMQLocalTransactionState.UNKNOWN;
        }
    }
}
