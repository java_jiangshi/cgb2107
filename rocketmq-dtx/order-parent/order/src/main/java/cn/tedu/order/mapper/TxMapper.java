package cn.tedu.order.mapper;

import cn.tedu.order.entity.TxInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TxMapper extends BaseMapper<TxInfo> {
    // 保存事务状态, insert()
    // 查询事务状态, selectById()
}
