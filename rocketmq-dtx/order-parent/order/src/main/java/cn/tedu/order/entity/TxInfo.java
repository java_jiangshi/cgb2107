package cn.tedu.order.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TxInfo {
    private String xid;       //事务id
    private Integer status;   //状态,0成功,1失败
    private Long created;     //创建时间
}
