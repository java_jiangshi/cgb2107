package cn.tedu.account.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/*
封装发送给账户的调用信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountMessage {
    private Long userId;
    private BigDecimal money;
    private String xid;
}
