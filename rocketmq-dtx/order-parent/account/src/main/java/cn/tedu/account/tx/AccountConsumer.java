package cn.tedu.account.tx;

import cn.tedu.account.entity.AccountMessage;
import cn.tedu.account.service.AccountService;
import cn.tedu.account.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RocketMQMessageListener(
        topic="orderTopic",
        consumerGroup="account-consumer")
public class AccountConsumer implements RocketMQListener<String> {
    @Autowired
    private AccountService accountService;

    @Override
    public void onMessage(String json) {
        // json --> AccountMessage
        AccountMessage am = JsonUtil.from(json, AccountMessage.class);
        accountService.decrease(am.getUserId(), am.getMoney());
        log.info("账户金额扣减成功");
    }
}
