package m2;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class Consumer {
    public static void main(String[] args) throws MQClientException {
        // 新建消费者
        DefaultMQPushConsumer c = new DefaultMQPushConsumer("c2");
        // 设置name server
        c.setNamesrvAddr("192.168.64.141:9876");
        // 订阅消息
        c.subscribe("Topic2", "*");
        // 设置处理消息的监听器, Orderly 监听器可以按顺序处理消息
        c.setMessageListener(new MessageListenerOrderly() {
            @Override
            public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs, ConsumeOrderlyContext context) {
                for (MessageExt msg : msgs) {
                    String s = new String(msg.getBody());
                    System.out.println("收到："+s);
                }
                return ConsumeOrderlyStatus.SUCCESS;
            }
        });
        // 启动
        c.start();
    }
}
