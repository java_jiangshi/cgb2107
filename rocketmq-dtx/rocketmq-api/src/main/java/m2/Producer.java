package m2;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

public class Producer {
    static String[] msgs = {
            "15103111039,创建",
            "15103111065,创建",
            "15103111039,付款",
            "15103117235,创建",
            "15103111065,付款",
            "15103117235,付款",
            "15103111065,完成",
            "15103111039,推送",
            "15103117235,完成",
            "15103111039,完成"
    };

    public static void main(String[] args) throws MQBrokerException, RemotingException, InterruptedException, MQClientException {
        // 新建生产者实例
        DefaultMQProducer p = new DefaultMQProducer("p2");
        // 设置 name server
        p.setNamesrvAddr("192.168.64.141:9876");
        // 启动
        p.start();

        // 遍历数组发送消息, 需要设置队列选择器
        for (String s : msgs) {
            // s ---- "15103111039,完成"
            Long orderId = Long.valueOf(s.split(",")[0]);

            // Topic2 可以在服务器上自动创建
            // Tag 标签可以省略
            Message msg = new Message("Topic2", s.getBytes());
            //p.send(消息, 队列选择器, 选择依据);
            SendResult r =
                    p.send(msg, (mqs, msg1, arg) -> {
                        // 三个参数： 队列的列表, 消息对象, 选择依据
                        Long oid = (Long) arg;
                        // 用订单id对队列数量取余，得到下标
                        int index = (int) (oid % mqs.size());
                        return mqs.get(index);
                    }, orderId);

            System.out.println(r);
        }
    }

}
