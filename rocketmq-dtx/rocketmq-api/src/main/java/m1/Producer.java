package m1;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.util.Scanner;

public class Producer {
    public static void main(String[] args) throws MQClientException, MQBrokerException, RemotingException, InterruptedException {
        // 新建生产者实例
        DefaultMQProducer p = new DefaultMQProducer("p1");
        // 设置 name server 地址
        p.setNamesrvAddr("192.168.64.141:9876");
        // 启动 （连接服务器）
        p.start();

        while (true) {
            System.out.print("输入消息：");
            String s = new Scanner(System.in).nextLine();
            // 把消息封装到 Message 对象
            // import org.apache.rocketmq.common.message.Message;
            // Topic 相当于是一级分类
            // Tag 相当于是二级分类
            Message msg = new Message("Topic1", "Tag1", s.getBytes());
            msg.setDelayTimeLevel(3);

            // 发送消息
            SendResult r = p.send(msg);
            // 打印服务器的反馈信息
            System.out.println(r);
        }

    }
}
