package m1;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class Consumer {
    public static void main(String[] args) throws MQClientException {
        // 新建消费者实例
        DefaultMQPushConsumer c = new DefaultMQPushConsumer("c1");
        // 设置 name server
        c.setNamesrvAddr("192.168.64.141:9876");

        // 订阅消息
        // *                        - 所有标签
        // Tag1 || Tag2 || Tag3     - 多种标签
        // Tag1                     - 一种标签
        c.subscribe("Topic1", "Tag1");

        // 设置处理消息的监听器
        // Concurrently - 多个线程并发接收处理消息
        c.setMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                for (MessageExt msg : msgs) {
                    String s = new String(msg.getBody());
                    System.out.println("收到："+s);
                }
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                /*
                告诉服务器重发消息
                服务器最多发送 18 次，每次发送时间间隔会越来越长，
                最长间隔 2 小时
                18次都处理失败，消息会进入死信队列
                 */
                // return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            }
        });

        // 启动
        c.start();
    }
}
