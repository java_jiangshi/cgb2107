package m3;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.Scanner;

public class Producer {
    public static void main(String[] args) throws MQClientException {
        // 新建事务消息生产者
        TransactionMQProducer p = new TransactionMQProducer("p3");
        // 设置 name server
        p.setNamesrvAddr("192.168.64.141:9876");
        // 设置事务消息监听器，发送事务消息会触发这个监听器执行
        p.setTransactionListener(new TransactionListener() {
            // 执行本地事务
            @Override
            public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
                // 消息， 业务数据参数
                System.out.println("-------------------------------------");

                if (Math.random() < 1) { // 100%
                    return LocalTransactionState.UNKNOW;
                }

                System.out.println("半消息发送后，执行本地事务");
                System.out.println("业务数据参数："+arg);
                if (Math.random() < 0.5) {
                    System.out.println("本地事务执行成功，提交消息");
                    return LocalTransactionState.COMMIT_MESSAGE;
                } else {
                    System.out.println("本地事务执行失败，回滚消息");
                    return LocalTransactionState.ROLLBACK_MESSAGE;
                }
                // 本地事务执行要么成功要么失败，应该不会出现未知状态
                // return LocalTransactionState.UNKNOW;
            }
            // 处理 Rocketmq 服务器的事务回查操作
            @Override
            public LocalTransactionState checkLocalTransaction(MessageExt msg) {
                // 1. 由于网络中断，上面方法事务状态无法通知服务器，
                // 2. 上面的本地事务执行缓慢，超过一分钟

                System.out.println("Rocketmq服务器正在回查事务状态");

                // 模拟网络中断，无法得到事务状态
                return LocalTransactionState.UNKNOW;
            }
        });
        // 启动
        p.start();
        // 发送事务消息（半消息），会触发一个监听器执行本地事务
        while (true) {
            System.out.print("输入消息：");
            String s = new Scanner(System.in).nextLine();
            Message msg = new Message("Topic3", s.getBytes());
            p.sendMessageInTransaction(msg, "传递给监听器的业务数据参数");
        }
    }
}
