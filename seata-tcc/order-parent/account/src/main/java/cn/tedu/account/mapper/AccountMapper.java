package cn.tedu.account.mapper;

import cn.tedu.account.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.math.BigDecimal;

public interface AccountMapper extends BaseMapper<Account> {
    //扣减账户金额
    void decrease(Long userId, BigDecimal money);

    //查询账户，用来判断是否有足够可用金额
    Account selectByUserId(Long userId);
    //可用 ---> 冻结
    void updateResidueToFrozen(Long userId, BigDecimal money);
    //冻结 ---> 已使用
    void updateFrozenToUsed(Long userId, BigDecimal money);
    //冻结 ---> 可用
    void updateFrozenToResidue(Long userId, BigDecimal money);
}
