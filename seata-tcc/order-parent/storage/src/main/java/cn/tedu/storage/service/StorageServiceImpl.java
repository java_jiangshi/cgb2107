package cn.tedu.storage.service;
import cn.tedu.storage.mapper.StorageMapper;
import cn.tedu.storage.tcc.StorageTccAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StorageServiceImpl implements StorageService {
    @Autowired
    private StorageTccAction tcc;

    @Override
    public void decrease(Long productId, Integer count) {
        // 不直接直接减库存的业务，而是冻结库存
        tcc.prepare(null, productId, count);
    }
}
