package cn.tedu.storage.controller;
import cn.tedu.storage.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class StorageController {
    @Autowired
    private StorageService storageService;
    // http://localhost:8082/decrease?productId=1&count=10
    @GetMapping("/decrease")
    public String decrease(Long productId, Integer count) {
        storageService.decrease(productId, count);
        log.info("productId="+productId);
        log.info("count="+count);
        return "减少库存成功";
    }
}
