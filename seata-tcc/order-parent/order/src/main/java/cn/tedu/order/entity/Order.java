package cn.tedu.order.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {
    private Long id;
    private Long userId;        //谁
    private Long productId;     //买了什么商品
    private Integer count;      //买多少件
    private BigDecimal money;   //花多少钱
    private Integer status;     //0-冻结,  1-正常
}
