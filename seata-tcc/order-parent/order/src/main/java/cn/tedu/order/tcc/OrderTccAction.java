package cn.tedu.order.tcc;

import cn.tedu.order.entity.Order;
import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

import java.math.BigDecimal;

/*
TCC 操作接口
定义三个操作的方法
 */

@LocalTCC
public interface OrderTccAction {
    // 为了避开 seata 的一个 bug，不传 Order 封装对象，
    // 而是把数据打散，一个个的单独传递

    @TwoPhaseBusinessAction(
            name = "OrderTccAction",
            commitMethod = "commit",
            rollbackMethod = "rollback")
    boolean prepare(BusinessActionContext ctx,
                    @BusinessActionContextParameter(paramName = "orderId") Long orderId,
                    Long userId,
                    Long productId,
                    Integer count,
                    BigDecimal money);

    boolean commit(BusinessActionContext ctx);
    boolean rollback(BusinessActionContext ctx);
}
