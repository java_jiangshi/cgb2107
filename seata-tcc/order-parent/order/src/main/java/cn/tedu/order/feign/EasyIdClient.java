package cn.tedu.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

// http://localhost:9090/segment/ids/next_id?businessType=order_business
@FeignClient(name = "EASY-ID")
public interface EasyIdClient {
    @GetMapping("/segment/ids/next_id")
    String getId(@RequestParam("businessType") String businessType);
}
