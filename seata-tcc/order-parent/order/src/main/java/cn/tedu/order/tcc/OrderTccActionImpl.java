package cn.tedu.order.tcc;

import cn.tedu.order.entity.Order;
import cn.tedu.order.mapper.OrderMapper;
import io.seata.rm.tcc.api.BusinessActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Component
public class OrderTccActionImpl implements OrderTccAction {
    @Autowired
    private OrderMapper orderMapper;
    @Transactional
    @Override
    public boolean prepare(BusinessActionContext ctx, Long orderId, Long userId, Long productId, Integer count, BigDecimal money) {
        orderMapper.insertFrozen(
                new Order(orderId,userId,productId,count,money,0));

        // 第一阶段成功时，添加一阶段成功的标记
        ResultHolder.setResult(OrderTccAction.class, ctx.getXid(), "p");

        return true;
    }
    @Transactional
    @Override
    public boolean commit(BusinessActionContext ctx) {
        // 一阶段成功标记不存在，二阶段不再重复执行
        if (ResultHolder.getResult(OrderTccAction.class, ctx.getXid()) == null) {
            return true;
        }

        Long orderId = Long.valueOf(ctx.getActionContext("orderId").toString());

        orderMapper.updateStatus(orderId, 1);
        // 二阶段执行成功时，删除标记
        ResultHolder.removeResult(OrderTccAction.class, ctx.getXid());
        return true;
    }
    @Transactional
    @Override
    public boolean rollback(BusinessActionContext ctx) {
        // 第一阶段成功冻结了数据，二阶段回滚时，才需要回滚恢复数据
        // 第一阶段失败，没有冻结数据，二阶段回滚动作也就不需要再执行
        if (ResultHolder.getResult(OrderTccAction.class, ctx.getXid()) == null) {
            return true;
        }

        Long orderId = Long.valueOf(ctx.getActionContext("orderId").toString());

        orderMapper.deleteById(orderId);
        ResultHolder.removeResult(OrderTccAction.class, ctx.getXid());
        return true;
    }
}
