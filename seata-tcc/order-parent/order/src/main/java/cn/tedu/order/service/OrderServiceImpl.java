package cn.tedu.order.service;
import cn.tedu.order.entity.Order;
import cn.tedu.order.feign.AccountClient;
import cn.tedu.order.feign.EasyIdClient;
import cn.tedu.order.feign.StorageClient;
import cn.tedu.order.mapper.OrderMapper;
import cn.tedu.order.tcc.OrderTccAction;
import cn.tedu.order.tcc.OrderTccActionImpl;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private EasyIdClient easyIdClient;
    @Autowired
    private AccountClient accountClient;
    @Autowired
    private StorageClient storageClient;
    @Autowired
    private OrderTccAction tcc;

    @GlobalTransactional // 启动全局事务
    @Override
    public void create(Order order) {
        // 远程调用id发号器，生产订单ID
        String s = easyIdClient.getId("order_business");
        Long orderId = Long.valueOf(s);
        order.setId(orderId);

        // 不再正常的创建订单，
        // 而是要手动调用 TccAction 的第一阶段方法，冻结订单
        // 第二阶段的两个方法，是由 seata 的 RM 接收协调器指令后自动执行
        // orderMapper.create(order);

        /*
         TccAction实例，是动态代理对象，用 AOP 切入了代码，
         在它的前置通知中会创建上下文对象，传入原始方法
         */
        tcc.prepare(null,
                order.getId(),
                order.getUserId(),
                order.getProductId(),
                order.getCount(),
                order.getMoney());

        // 远程调用库存，减少库存
        storageClient.decrease(order.getProductId(),order.getCount());
        // 远程调用账户，扣减金额
        accountClient.decrease(order.getUserId(),order.getMoney());
    }
}
