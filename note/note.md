[toc]



# 随堂笔记

这是随堂笔记, 详细内容请参考在线笔记:
https://blog.csdn.net/weixin_38305440

# 笔记乱码问题



![image-20200613174552258](note.assets/image-20200613174552258.png)

<video id="video" controls="" preload="none">
    <source id="mp4" src="note.assets/a.mp4" type="video/mp4">
</video>














# 课程安排(17到18天)

1. Spring Cloud Netflix（6）
2. RabbitMQ（2）
3. 分布式事务（3）
7. RocketMQ（1）
6. Docker（2）
7. Elasticsearch
8. Kubernetes（3）



王海涛





# 开发环境

- IDEA
- Lombok
- EditStarters
- Free MyBatis Plugin
- Maven Helper
- Maven
  - 使用阿里仓库和中央仓库，来回切换更新
    - settings.xml 不配置任何镜像仓库，默认使用中央仓库
  - 找到本地仓库的依赖包，删除后重新更新
  - 复制同学的本地仓库中的依赖包
- 新工程的  Maven 默认配置：
  - File - New Projects Settings - Settings for new projects
- springboot版本：  2.3.2.RELEASE
- springcloud版本： Hoxton.SR12





# 常用地址

提问：

- 直播聊天
- 微信： wanght6666666

笔记

http://wanght.blog.csdn.net

https://blog.csdn.net/weixin_38305440/article/details/108609574



随堂笔记

http://code.tarena.com.cn/CGBCode/cgb2107/

http://218.247.142.198/CGBCode/cgb2107/

tarenacode

code_2013



Gitee代码仓库

https://gitee.com/benwang6/cgb2107



课前资料，百度网盘:
https://pan.baidu.com/s/19tOuVOYJsplssj3kLvfzcA



# Spring Cloud Netflix



- 注册中心
  - Nacos
  - Eureka
- 配置中心
  - Nacos
  - Spring Cloud Config / BUS
- 远程调用、负载均衡
  - Feign、Ribbon
- 系统容错、限流
  - Sentinel
  - Hystrix
- API网关
  - Spring Cloud Gateway
  - Zuul
- 数据监控
  - Sentinel
  - Hystrix Dashboard+Turbine
  - 链路跟踪监控：Sleuth+Zipkin





# Spring cloud 应用案例

**新建工程**

1. 新建 maven 工程： springcloud1
2. 配置 pom.xml，设置 springboot 和 springcloud 版本
   - springboot 2.3.2.RELEASE
   - springcloud Hoxton.SR12



**通用模块** 

1. 新建 maven 模块： sp01-commons
2. pom.xml 从笔记复制
3. 三个实体类
4. 三个业务接口
5. 三个工具类



**三个业务模块**

1. 新建 Maven 模块，继承 springcloud1
2. 添加 spring web 依赖、commons
3. application.yml
   -  app.name
   - port
4. Controller
5. Service



# Eureka 注册中心

**搭建 Eureka 配置中心**

1. 新建 spring 模块： sp05-eureka
2. pom.xml 修改，继承 springcloud1，添加 eureka server 依赖
3. yml
   - 禁用自我保护模式
   - 主机名
   - 针对单台服务器，不注册也不拉取
4.  启动类注解： `@EnableEurekaServer` 通过注解来触发 eureka 服务器的自动配置
5. 浏览器访问注册表： `http://localhost:2001/`



**eureka运行机制**

- 注册

  客户端向服务器注册时，会一次次反复注册，直到注册成功为止

- 拉取

  客户端每30秒拉取一次注册表，更新本地的缓存的注册表

- 心跳

  客户端每30秒发送一次心跳数据，服务器连续三次收不到一个服务的心跳，就会删除该服务

- 自我保护模式

  - 在网络中断时，15分钟内，85%服务器出现心跳异常，自动进入自我保护模式
  - 保护模式下，所有注册信息都不删除
  - 网络恢复后，可以自动退出保护模式
  - 开发调试期间，可以禁用保护模式，避免影响测试

  

**客户端连接 eureka**

修改 2,3,4 

1. 添加 eureka client 依赖
2. yml 配置 eureka 地址
   - http://eureka1:2001/eureka
3. 修改 hosts，添加 eureka1 和 eureka2
   - win+r，输入 drivers
   - 进入 etc 目录，找到 hosts 文件
   - 用管理员运行启动编辑器，修改 hosts 文件



# 高可用

**商品的高可用**

1. 修改 item 的启动配置，添加 springboot 启动参数：`--server.port=8001`
2.  复制启动配置，修改端口 `8002`



**eureka高可用**

1. 添加 eureka1 和 eureka2 的 profile 配置

2. 配置启动参数

   ```
   # eureka1
   --spring.profiles.active=eureka1  --server.port=2001
   
   # eureka2
   --spring.profiles.active=eureka2  --server.port=2002
   ```

3. 修改 2,3,4，同时连接两台eureka服务器

   - http://eureka1:2001/eureka, http://eureka2:2002/eureka



# Feign 远程调用，和 Ribbon 负载均衡和重试



**04订单，远程调用商品和用户**

1. 添加 feign 依赖
2. 启动类添加注解 `@EnableFeignClients`
3. 定义远程调用接口
   - ItemClient
   - UserClient
4. 修改 OrderServiceImpl ，完成远程调用



## Feign 集成的 Ribbon

负载均衡和重试



**Ribbon 负载均衡**

- Feign 集成 Ribbon，默认启动负载均衡



**Ribbon 重试**

- 调用后台服务时，如果失败可以自动发起重试调用

  - 出异常
  - 调用超时
  - 服务器宕机

- Feign 集成 Ribbon，默认启动了重试

- 重试参数：

  - ribbon.MaxAutoRetries
    单台服务器的重试次数，默认 0

  - ribbon.MaxAutoRetriesNextServer

    更换服务器的次数，默认 1

  - ribbon.ReadTimeout

    默认 1000 毫秒超时

  - ribbon.ConnectTimeout
    与后台服务器建立连接的超时时间，默认 1000

  - ribbon.OkToRetryOnAllOperations
    是否对所有请求方式都重试，默认只对 GET 重试



# Zuul API 网关

- 统一的入口
- 统一的权限校验
- 集成 Ribbon
- 集成 Hystrix



**统一的入口**

1. 新建 spring 模块： sp06-zuul

2. 添加依赖： 

   - eureka client

   - zuul
   - sp01

3. yml

   ```yml
   # 转发规则
   zuul:
     routes:
     	# ** 包含深层子路径
     	# *  只包含一层路径
     	# service-id 作为访问子路径，是默认设置
     	# 根据注册表中的注册信息，zuul可以自动配置
     	# 最好自己手动配置，防止注册表不全
       item-service: /item-service/**
       user-service: /user-service/**
       order-service: /order-service/**
   ```

4. 启动类注解：`@EnableZuulProxy`



**统一的权限校验**

zuul 的过滤器 ZuulProxy，可以过滤客户端请求，在过滤器中可以检查访问权限



http://localhost:3001/item-service/iuy4tgf3		没有登录，不允许访问

http://localhost:3001/item-service/iuy4tgf3?token=iu5y4t3tg3		已登录，可以访问

1. 新建过滤器类：AccessFilter  按照 Zuul 的规则实现
2. 添加注解 `@Component`

- zuul的自动配置，会在 spring 容器中发现过滤器实例，完成自动配置



**Zuul集成Ribbon**

- 负载均衡默认启用

- 重试默认禁用

  在最前面重试，能能造成后台服务器大面积压力倍增，大面积出现故障

- 启用重试

  - 添加 spring-retry 依赖
  - yml 配置启用重试： `zuul.retryable=true`
  - 如果需要可以配置重试参数

  

# Zuul 集成 Hystrix

Hystrix是容错和限流工具

- Hystrix容错：降级
- Hystrix限流：熔断



**Zuul 网关使用 Hystrix 进行容错处理，执行降级**

Zuul 默认已经启用Hystrix，任何基础配置都不用做

调用后台服务失败，执行前面模块中的降级代码，向客户端返回**降级结果**

- 错误提示
- 缓存数据
- 根据业务逻辑，返回任何结果都可以



1. 新建降级类  ItemFB，实现 FallbackProvider 接口
2. 添加注解 `@Component`

- Zuul的自动配置，可以自动发现降级类实例，完成自动配置



**Zuul 集成 Hystrix 实现限流，熔断**

 当流量过大，后台服务出现故障，可以断开链路，限制后台故障服务的流量，等待它从故障中恢复

- 断路器打开条件：
  - 10秒20次请求（必须首先满足）
  - 50%请求出错，执行了降级代码
- 断路器打开后，会进入**半开状态**
  在半开状态下，会向后台服务尝试发送一次客户端调用，
  调用成功，自动关闭断路器恢复正常，
  调用失败，继续保持打开状态



# Hystrix dashboard

对 Hystrix  降级和熔断的情况进行监控，可以通过监控快速定位故障模块



**Hystrix 使用 Actuator 暴露自己的监控日志**

Actuator是 springboot 提供的项目监控指标工具，可以暴露项目的多种监控指标

- 健康状态
- spring容器中所有的对象
- spring mvc  映射的所有路径
- 环境变量
-  堆内存镜像
- .....



**暴露Actuator监控指标**

1. actuator 依赖

2. yml 

   ```yml
   m.e.w.e.i="*"			#暴露所有监控指标
   
   m.e.w.e.i=health,beans,mappings			#暴露所有监控指标
   
   m.e.w.e.i=hystrix.stream
   ```

3. 访问

   `http://localhost:3001/actuator`



**搭建 Hystrix-dashboard 仪表盘**

1. 新建 spring 模块： sp07-hystrix-dashboard

2. 调整pom.xml，添加 hystrix dashboard 依赖

3. yml

   ```yml
   允许抓取日志的服务器列表
   ```

4. 启动类注解：`@EnableHystrixDashboard`

5. 访问

   ```
   http://localhost:4001/hystrix
   
   输入框填入 http://localhost:3001/actuator/hystrix.stream
   ```



**06网关高可用**

设置两个启动配置

```shell
Sp06Zuul-3001
--server.port=3001

Sp06Zuul-3002
--server.port=3002
```



# Turbine

从多态服务器聚合 Hystrix 监控数据，Hystrix dashboard仪表盘可以从Turbine抓取聚合后的日志数据

1. 新建 spring 模块： sp08-turbine

2. pom.xml 添加 eureka client、turbine

3. yml

   ```yml
   聚合的服务的id列表： zuul,aaa,bbb,ccc
   为聚合后的数据命名： new String("aaa")
   ```

4. 启动类注解： `@EnalbeTurbine`

5. 访问日志数据： `http://localhost:5001/turbine.stream`



# Springcloud Config 配置中心

集中管理和维护配置文件



**准备 Git 仓库**

1. springcloud1工程目录下，新建 config 文件夹
2. 复制 2,3,4 的 application.yml 到 config 目录
   - item-service-dev.yml
   - user-service-dev.yml
   - order-service-dev.yml



**创建本地git仓库**

已经创建过仓库的同学不要重复创建

1. vcs -- create git repository
   （double shift 搜索create git repository）
2. 选择 springcloud1 工程目录，作为仓库目录
3. 把代码提交到本地仓库
   ctrl+k
   或点右上角对勾按钮
   选择所有文件，填写提交信息，完成提交



**本地仓库推送到远程仓库**

已经有远程仓库不要重复创建

1. gitee 中点加号下拉菜单，新建仓库
2. 仓库名称： springcloud1
3. 设置成开源仓库
4. 创建完成后，复制仓库地址
5. 推送
   ctrl+shift+k
   或点右上角向上箭头按钮
6. 点 define remote 链接，粘贴gitee仓库地址，完成推送



## 搭建配置中心

1. 新建 spring 模块： sp09-config

2. 添加依赖：eureka client，config server

3. yml

   ```yml
   赵一凡
   git仓库地址： https://gitee.com/zhao-yifan/springcloud1
   存放配置文件的目录： config
   
   git仓库地址： https://gitee.com/benwang6/cgb2107
   存放配置文件的目录： springcloud1/config
   ```

4. 启动类注解 `@EnableConfigServer`

5. 访问验证：

   - `http://localhost:6001/item-service/dev`
   - `http://localhost:6001/user-service/dev`
   - `http://localhost:6001/order-service/dev`
   - `http://eureka1:2001` 查看注册表有没有 `config-server`



## 配置中心的客户端模块

修改 2,3,4 连接配置中心，下载配置

1. 删除 application.yml （或把代码全部注释掉）

2. 添加依赖： config client

3. bootstrap.yml

   ```yml
   # 从注册表得到配置中心的地址，再从配置中心下载配置文件
   
   eureka 地址
   指定配置中心的服务id
   下载指定的配置文件：
    - user-service
    - dev
   ```

   

![image-20211119092013815](note.assets/image-20211119092013815.png)



# VMware

- 16.x
- NAT 网络的网段使用 `192.168.64.0`
  编辑 -- 虚拟网络编辑器 -- 选择 vmnet8 - 左下角修改 `192.168.64.0`



**虚拟机**

课前资料/虚拟机/

- centos-7-1908
- centos-8-2105 (如果不能使用 centos-8，用上面的centos-7)
- 上面虚拟机中已经做了基本基础配置
  -  yun安装源、扩展源使用阿里服务器
  - 安装了 python、pip、ansible
  - 添加了两个脚本文件，方便配置ip地址
    - ip-static - 配置固定ip
    - ip-dhcp - 自动获取ip



1. 解压 centos-8-2105
2. 双击 centos-8-2105.vmx 加载镜像
3. 启动，按提示选择“已复制虚拟机”
4. 如果提示“硬件兼容性”，右键点虚拟机，管理，修改硬件兼容性，改成你的vmware 版本
5. 登录用户名密码都是 `root`



**ip设置测试**

1. `./ip-dhcp`自动获取ip



**如果网卡有问题，不能设置ip**

```shell
# centos 7 禁用 NetworkManager 系统服务
systemctl stop NetworkManager
systemctl disable NetworkManager

# centos 8 开启 VMware 托管
nmcli n on
systemctl restart NetworkManager

如果网络还有问题，可以重置 vmware 的虚拟网络
编辑 -- 虚拟网络编辑器 -- 还原默认设置

还原默认设置会删除所有的虚拟网络，重新创建，重新初始化
```



**准备Docker环境**

1. 关闭 centos-8-2105
2. 克隆centos-8-2105： docker-base
3. mobaxterm 连接 docker-base 上传文件到 /root/
   - 课前资料/DevOps课前资料/Docker/docker-install/  文件夹
4. 参考 CSDN docker离线安装笔记，从第 3 步开始安装
5. 关机 `shutdown -h now`



# Rabbitmq

消息队列

消息服务器

消息中间件 Borker

- Rabbitmq
- Activemq
- Rocketmq
- Kafka
- Tubemq



**Docker运行Rabbitmq**

1. 克隆 docker-base： rabbitmq

2. 设置ip

   ```shell
   ./ip-static
   ip: 192.168.64.140
   
   ifconfig
   ```

3. 上传 rabbitmq 镜像文件到 /root/

   - DevOps课前资料/Docker/rabbit-image.gz

4. 导入镜像 `docker load -i rabbit-image.gz`

5. 按照 CSDN  rabbitmq 笔记，找到 "安装 -- docker 运行 rabbitmq" 来启动 rabbitmq



# 消息服务案例

1. BUS配置刷新
   刷新指令消息发送到Rabbitmq ，其他模块接收执行，执行刷新操作
   主题模式
2. sleuth + zipkin 链路跟踪
   链路跟踪日志，通过 Rabbitmq  中转发送到 Zipkin 服务器
   简单模式
3. 订单的流量削峰
   购物系统产生的订单，不直接存储到数据库，而是发送到 Rabbitmq，
   后台的消费者模块接收订单，再向数据库存储，
   短时间大量订单并发存储，变成顺序存储
   简单模式 / 工作模式
4. Rocketmq 异步调用，可靠消息最终一致性事务
   订单通过消息服务，异步的调用账户，使用 Rocketmq 的事务消息实现事务控制



# BUS  配置刷新

消息总线

动态配置刷新，通过配置中心向 MQ 服务发送一个刷新指令，其他配置中心客户端接收指令，执行配置刷新

1. 修改 2,3,4,9 添加依赖：

   - bus
   - rabbitmq
   - binder-rabbit

2. 修改09，添加 actuator 依赖

3. 修改 09 的 yml 配置

   - 暴露 bus-refresh 刷新路径
   - rabbitmq 连接配置

4. 修改 2,3,4 的 yml 配置，添加 rabbitmq 连接

   - 修改 config 目录的配置文件，提交推送到远程仓库

5. 重启09

6. 重启 2,3,4

   

   排查错误

   ```
   启动顺序：
   05
   09
   
   02
   03
   04
   
   06
   
   07
   08
   
   2,3,4 控制台必须有连接 6001 的日志
   http://localhost:6001/item-service/dev
   http://localhost:6001/user-service/dev
   http://localhost:6001/order-service/dev
   
   访问 http://eureka1:2001 注册表
   eureka-server
   config-server
   item-service
   user-service
   order-server
   zuul
   
   
   访问 http://localhost:6001/actuator
   是否有 bus-refresh 刷新路径
   
   访问 rabbitmq 服务器，查看是否有 connection、chanel，是否有 springCloudBus 交换机
   ```

   

   **配置刷新测试**

   1. 向 http://localhost:6001/actuator/bus-refresh 提交一个 POST 请求
   2. 观察 2,3,4 控制台是否重新连接 6001，刷新配置

   

   **测试 03 的配置添加新用户，然后让 03 刷新配置**

   1. 修改 UserServiceImpl，添加 @RefreshScope
      如果不添加这个注解，即使刷新到新的配置，也不会向对象重新注入新配置
   2. 重启 03
   3. 修改 config 目录的 user-service-dev.yml，添加一个 99 用户
   4. 提交推送到远程仓库
   5. 让 03 刷新配置 `POST http://localhost:6001/actuator/bus-refresh/user-service`
   6. 访问新的用户数据 `GET http://localhost:3001/user-service/99`



# 选择正确网卡，注册ip不注册主机名

**选择正确网卡**

bootstrap.yml

```yml
      ignored-interfaces: # 忽略的网卡
        - VM.*            # VM开头， .是任意字符， *0到多个
      preferred-networks: # 要是用的网卡的网段
        - 192\.168\.0\..+ # \.是普通的.字符， .+任意字符1到多个
```



**注册ip，而不注册主机名**

application.yml

```yml
prefer-ip-address: true # 使用ip进行注册

虽然注册了ip，但eureka注册表中看到的 instance id 可能还是主机名
instance-id: ${spring.cloud.client.ip-address}:${spring.application.name}:${server.port} # 界面列表中显示的格式也显示ip
```





# Sleuth + Zipkin 链路跟踪

**Sleuth** 

产生链路跟踪日志

A --> B --> C

```
A, I6U5HYG4TFTY, I6U5HYG4TFTY, true
B, I6U5HYG4TFTY, 656HG43GHJ54, true
C, I6U5HYG4TFTY, JH4G3TG46UJ5, true
```



- 修改 2,3,4,6，添加 sleuth 依赖

- sleuth 是 0 配置，它有自动配置类



 **链路日志发送到 Zipkin**

修改 2,3,4,6，日志通过 Rabbitmq 发送到 Zipkin 服务器

1. 添加 zipkin client 依赖
2. 在 06 项目添加 Rabbitmq 依赖和 Rabbitmq 连接配置
3. 四个模块中，配置发送方式： rabbit
   - 2,3,4 在 config 目录中修改，然后推送到远程仓库



**启动 zipkin 服务器**

1.  

   ```
   java -jar z.jar --zipkin.collector.rabbitmq.uri=amqp://admin:admin@192.168.64.140:5672
   
   java -jar z.jar --zipkin.collector.rabbitmq.uri=amqp://admin:admin@wht6.cn:5672
   
   java -jar z.jar --zipkin.collector.rabbitmq.uri=amqp://admin:admin@wht6.cn:5672/空间
   ```

   

# Rabbitmq  订单流量削峰案例

**导入商城项目**

1. 课前资料/elasticsearch/pd-商城项目案例.zip
   里面的 pd-web 文件夹，解压到 rabbitmq 工程目录

2. pom.xml 拖拽到 idea，把  springboot 版本改成 2.3.2.RELEASE

3. 右键点pom.xml编辑器，选择 add as maven project

4. sqlyog，右键点连接，从sql 转储文件导入
   找到 pd-web 项目目录中的 pd.sql

   - 如果导入失败，可以增大mysql缓存区

     ```sql
     set global max_allowed_packet=100000000;
     set global net_buffer_length=100000;
     SET GLOBAL  interactive_timeout=28800000;
     SET GLOBAL  wait_timeout=28800000
     ```

   - 确认

     ```
     共13张表
     
     SELECT COUNT(1) FROM pd_item
     3160
     
     SELECT COUNT(1) FROM pd_item_desc
     3162
     ```

5. pd-web 的 application.yml 查看是否需要调整数据库连接配置（用户密码）

6. 执行启动类 RunPdAPP

7. 调整启动配置，在 working directory 中配置  pd-web 模块目录路径
   然后重新启动

8. 访问 http://localhost



**订单发送到 Rabbitmq**

1. 添加 Rabbitmq 依赖和 Rabbitmq 连接配置
2. 在启动类（或者自定义自动配置类）中设置队列的参数： orderQueue, true, false, false
3. 修改 OrderServiceImpl
   - 注入对象： AmqpTemplate（用来封装发送消息代码的工具）
   - 发送消息



**订单的消费者**

1. Rabbitmq基础配置，准备队列参数
2. 新建消费者类： OrderConsumer 
3. 用注解配置接收消息
4. 收到的订单通过原来的业务方法代码，存储到数据库



# 分布式事务

## 业务案例

**数据库初始化工具**

1. 新建 spring 模块：db-init
2. 添加依赖：
   - spring jdbc -- ScriptUtils
   - mysql driver
3. yml 配置数据库连接
4. 在 resources/sql/ 添加 4个 sql 脚本文件
   - Mysql 5.x 修改修改脚本文件
   - DATETIME(6)  ---> DATETIME
   - order.sql 最后一行的 NOW() 改成 0
5. 在启动类中添加代码，执行这4个sql脚本



**eureka注册中心**

1. 新建 spring 模块 : eureka
2. 添加 eureka server 依赖
3. yml
   - 禁用保护模式
   - 单台服务器不注册、不拉取
   - 端口： 8761
4. 启动类添加注解：`@EnableEurekaServer`



**父项目**

统一管理依赖

1. 新建 Maven 模块：order-parent
2. 从笔记完整复制 pom.xml
3. 把父项目的 src 目录删掉



**业务模块** -- account、storage、order

1. 新建 spring 模块
2. 调整pom.xml，从  order-parent 继承
3. yml配置
   - application
   - bootstrap
4. 实体类
5. Mapper
6. 启动类添加 `@MapperScan`
7. Service
8. Controller



**全局唯一id发号器**

1. 下载项目解压到工程目录
2. 修改pom.xml，导入项目
3. 添加 eureka client 依赖
4. 添加 bootstrap.yml
5. applicatoin.yml 配置 eureka



**订单远程调用库存、账户和发号器**

1. feign依赖
2. 启动类注解 `@EnableFeignClients`
3. 添加三个远程调用接口
   - StorageClient
   - AccountClient
   - EasyIdClient
4. 修改 OrderServiceImpl 完成远程调用



## Seata AT 事务

**启动 TC （事务协调器）： Seata Server**

1. 课前资料/分布式事务/seata-server-1.3.0.zip
   解压缩

2. 修改三个配置文件

   - registry.conf
     向 eureka 注册
   - file.conf
     seata server运行过程中产生的日志数据，存储到什么位置
   - seata-server.bat （苹果电脑用seata-server.sh）
     修改虚拟机使用的内存大小

3. 运行 seata-server.bat 启动服务

   - JAVA_HOME、PATH
   - JDK 必须用 1.8
   - 命令行窗口不能关闭
   - 窗口中内容不能选中，否则应用会被挂起，暂停执行

   

**添加 Seata At 事务**

1. 添加 seata 依赖
2. 修改三个配置文件：
   - application.yml
     设置事务组的组名
   - registry.conf
     设置注册中心的地址
   - file.conf
     事务组对应使用的协调器
3. 新建自动配置类，创建数据源代理
4. 在业务方法上添加事务注解
   - `@Transactional` --  控制本地事务
   - `@GlobalTransactional` --  启动全局事务，只在第一个模块添加



## TCC 事务

有侵入的事务方案

80% 业务场景都可以使用 AT 事务，

在一些复杂情况下（自定义数据库函数、自定义数据库存储过程），无法使用 AT 事务自动控制事务执行

**TCC --  两个阶段的三个事务操作**

- Try - 预留资源，冻结数据

--------------------------

- Confirm - 确认资源，使用第一阶段冻结的数据来完成业务数据处理，commit
- Cancel - 取消预留的资源，把一阶段冻结的数据，恢复回去， rollback



**导入项目案例**

1. 新建 Empty project 工程： seata-tcc
2. 无事务版本.zip，解压到 seata-tcc  文件夹下
3. 导入模块



 **在订单案例中添加 TCC 事务**

1. seata 依赖
2. 三个配置文件，与 AT 事务完全相同
   - application.yml -- 组名
   - registry.conf -- 注册中心地址
   - file.conf -- 组对应使用的协调器
3. 修改 Mapper，添加新的数据操作
4. 添加 ResultHolder 类，用来控制幂等性
5. 按照 seata 的规则，添加 TccAction  接口和实现，使用 Mapper 完成 TCC 数据库操作
6. 修改业务方法，调用 TccAction 的第一阶段方法来冻结数据
7. 在第一个模块上天 `@GlobalTransactional`   启动全局事务



 **两个问题**

- 如果第一阶段冻结数据失败，仍然会执行二阶段的回滚操作
- 第二阶段失败，TC会重复下发二阶段执行指令，重复执行二阶段的数据操作



**幂等性控制**

幂等性：重复操作时，和一次操作结果相同



## 可靠消息最终一致性事务

Rocketmq 的高可靠消息、事务消息，实现在异步调用场景下，保证消息的正确传递，模块的正确执行



**导入项目**

1. seata-at/无事务版本.zip，解压到 rocketmq-dtx 工程目录
2. 导入模块



**订单通过 Rocketmq 异步调用账户**

1. 父项目添加  rocketmq 依赖
2. yml 配置 name server 地址，生产者需要设置生产者组名
3. 添加新的数据表：tx_table，用来保本本地事务的执行状态
4. 添加实体类： TxInfo 封装事务状态数据
5. 添加 TxMapper
6. 新建 AccountMessage  对象，用来封装账户的调用数据
7.  添加 JsonUtil 工具类，用来处理 json 格式转换
8. 修改 OrderServiceImpl
   - 不再直接执行业务代码
   - 发送事务消息
   - 实现事务消息监听器，发送消息后会触发这个监听器执行本地事务（订单存储业务）



**账户接收消息，执行金额扣减**

1. yml 配置 name server
2. AccountMessage，收到的 json 数据要转成这个对象
3. JsonUtil
4. 新建消费者类： AccountConsumer，通过注解配置接收消息
5. 执行 AccountService，扣减账户





# Rocketmq

- 搭建Rocketmq服务器

  1. 克隆  centos-8-2015： rocketmq

  2. 设置ip

     ```shell
     ./ip-static
     ip: 192.168.64.141
     ```

  3. 上传文件到 /root/

     - 课前资料\分布式事务\rocketmq\  文件夹下的三个文件

- rocketmq 启动命令

  ```shell
  cd /usr/local/rocketmq/
  
  nohup sh bin/mqnamesrv &
  
  nohup sh bin/mqbroker -n localhost:9876 &
  
  如果需要从配置文件启动 broker：
  nohup sh bin/mqbroker -n localhost:9876 -c conf/broker.conf &
  
  cd ~/
  
  nohup java -jar rocketmq-console-ng-1.0.1.jar --server.port=8080 --rocketmq.config.namesrvAddr=localhost:9876 &
  
  jps
  
  54801 NamesrvStartup
  56038 BrokerStartup
  64331 jar
  ```

  

- Rocketmq基本概念： 

  - nameserver
  - broker
  - topic

- 消息收发方式：

  - 同步消息
  - 延时消息
  - 顺序消息
  - 事务消息



1. 新建 Empty project 工程： rocketmq-dtx

2. 新建 Maven 模块： rocketmq-api

3. 修改 pom.xml，添加 rocketmq 依赖

   

# Docker

1. 克隆 docker-base：docker

2. 设置ip

   ```shell
   ./ip-static
   ip: 192.168.64.150
   ```

3. 上传文件到 /root/

   - 课前资料/DevOps课前资料/docker/docker-images.gz
   - 课前资料/DevOps课前资料/docker/tomcat 文件夹

4. 导入镜像

   ```
   docker load -i docker-images.gz
   docker images
   ```

   

   

   **自己构建的tomcat:10，部署一个 Hello world 应用**

   ```shell
   mkdir /opt/web
   
   cat <<EOF > /opt/web/index.jsp 
   <h1> Hello world! </h1>
   <h1> Hello \${param.name}! </h1>
   EOF
   
   docker run -d --name web \
   --restart=always \
   -v /opt/web:/usr/local/apache-tomcat-10.0.6/webapps/ROOT \
   -p 80:8080 \
   tomcat:10
   
   浏览器访问
   http://192.168.64.150/
   http://192.168.64.150/?name=Zhangsan
   
   ```

   

   # Elasticsearch

   **Docker搭建ES服务器集群**

   1. 克隆 docker-base： es

   2. 设置ip

      ```shell
      ./ip-static
      ip: 192.168.64.181
      ```

   3. 上传文件到 /root/

      - elasticsearch/pditems 文件夹
      - elasticsearch-analysis-ik-7.9.3.zip
      - es-img.gz

   4. 导入镜像：`docker load -i es-img.gz`

   5. 关闭防火墙

      ```shell
      systemctl stop firewalld
      systemctl disable firewalld
      ```

   6. 设置虚拟机内存 2G+

   7. 重启虚拟机

   

# Spring data elasticsearch

1. 新建 spring 工程： es
2. 添加 elasticsearch 依赖
3. yml 配置服务器地址列表
4. 实体类：Student
5. 新建 Repository 接口： StudentRepository
   (与 Mapper 作用相同，都是访问底层数据库的数据)
6. 写测试方法，使用 Repository 接口做数据增删改查测试





**拼多商城搜索条**

1. 添加 es 依赖
2. yml 配置  es 服务器
3. 新建实体类 Item，封装商品数据
4. 定义 ItemRepository 接口，添加方法在商品名称和卖点中搜索关键词
5. SearchService
6. SearchController
7. search.jsp



# Kubernetes

容器的自动部署工具

谷歌的开源工具



K8s的安装非常复杂，有开源工具可以辅助安装搭建K8s集群

- 一键安装脚本
- 一步一步手把手安装



**集群方案**

- 16G或以上内存： 三个虚拟机 ，每个虚拟机内存至少 2G
- 8G： 两个虚拟机，每个虚拟机内存至少 2G



**第一台服务器**

1. 克隆 centos-8-2105： k1

2. 设置 ip

   ```shell
   ./ip-static
   ip: 192.168.64.191
   ```

3. 上传文件

   - DevOps课前资料\kubernetes\kubeasz-3.1.0.zip  解压缩
   - kubeasz 文件夹上传到 /etc/ 目录
   - ezdown 文件上传到 /root/ 目录
   - images.gz 上传到 /root/ 目录

4. 按 csdn 笔记步骤做

   1. 调整 VMware 虚拟机的内存和 cpu：
      内存用2G或以上，cpu 用2块

   2. 安装docker

      ```shell
      cd ~/
      chmod +x ./ezdown
      ./ezdown -D
      ```

   3. 加载镜像

      ```shell
      docker load -i images.gz
      docker images
      ```

   4. 重启服务器 `shutdown -r now`



**工作节点服务器**

1. 从 k1 克隆：  k2 和 k3

2. 设置 k2 和 k3 的ip

   - 192.168.64.192
   - 192.168.64.193

3. 在191继续执行下面步骤：

   1. 启动临时容器

      ```shell
      ./ezdown -S
      ```

   2. 安装方式设置成离线安装

      ```shell
      sed -i 's/^INSTALL_SOURCE.*$/INSTALL_SOURCE: "offline"/g' /etc/kubeasz/example/config.yml 
      ```

   3. 配置免密登录

      ```shell
      ssh-keygen -t rsa -b 2048 -N '' -f ~/.ssh/id_rsa
      
      ssh-copy-id 192.168.64.191
      
      ssh-copy-id 192.168.64.192
      
      ssh-copy-id 192.168.64.193
      ```

   4. 创建集群方案

      ```shell
      cd /etc/kubeasz
      
      chmod +x ezctl
      # 在新建的集群方案目录下，会放入默认的样例设置
      ./ezctl new cs1
      ```

   5. 按照笔记修改方案设置

   6. 三台服务器拍摄快照，保留当前状态，如果后面安装失败，可以快速回退到当前状态

   7. 一键安装

      ```shell
      cd /etc/kubeasz
      
      ./ezctl setup cs1 all
      ```



# 小结

- Spring cloud netflix
  - eureka
  - 配置中心 spring cloud config + bus
  - Feign + Ribbon
  - Hystrix
  - Hystrix Dashboard + Turbine
  - Zuul
  - Sleuth + zipkin
- 消息服务
  - 使用场景：解耦、削峰、异步调用
    - bus 刷新指令
    - sleuth + zipkin 链路跟踪日志
    - 订单流量削峰
    - 异步调用，使用 Rocketmq 实现可靠消息最终一致性事务
  - Rabbitmq
    - 六种模式：简单、工作、发布和订阅、路由、主题、RPC
  - Rocketmq - 事务消息、高可靠消息
- 分布式事务
  - Seata
  - 三个组件：
    - TC - 事务协调器
    - TM - 事务管理器
    - RM - 资源管理器
  - AT - 全自动事务，对业务无侵入
  - TCC - 手动控制事务的方案，有侵入
  - Rocketmq 可靠消息最终一致性事务
- Docker + Kubernetes
  - 开发运维一体化（DevOps）核心工具
  - 控制器
  - 容器
  - Service
  - Deployment
- Elasticsearch
  - 分布式搜索引擎
  - NoSql 数据库
  - 核心概念：倒排索引、分片、副本、映射、类别（淘汰）、文档、字段
  - Spring Data Elasticsearch -- Repository API







