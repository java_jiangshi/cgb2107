package test;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisShardInfo;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;

import java.util.ArrayList;
import java.util.List;

public class Test1 {
    public static void main(String[] args) {
        // 服务器的列表
        List<JedisShardInfo> list = new ArrayList<>();
        list.add(new JedisShardInfo("192.168.64.150", 7000));
        list.add(new JedisShardInfo("192.168.64.150", 7001));
        list.add(new JedisShardInfo("192.168.64.150", 7002));
        // 配置对象
        GenericObjectPoolConfig conf = new JedisPoolConfig();
        // 分片连接池
        ShardedJedisPool pool = new ShardedJedisPool(conf, list);
        // 创建数据操作工具对象
        ShardedJedis j = pool.getResource();
        // 放 100 条数据
        for (int i = 0; i < 100; i++) {
            j.set("k"+i, "v"+i);
        }
        pool.close();


    }
}
