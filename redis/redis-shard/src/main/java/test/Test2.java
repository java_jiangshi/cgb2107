package test;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Set;

public class Test2 {
    public static void main(String[] args) {
        // 哨兵服务地址列表
        Set<String> set = new HashSet<>();
        set.add("192.168.64.150:5000");
        set.add("192.168.64.150:5001");
        set.add("192.168.64.150:5002");
        // 配置对象
        JedisPoolConfig conf = new JedisPoolConfig();
        // 哨兵连接池
        JedisSentinelPool pool =
            new JedisSentinelPool("mymaster", set, conf);
        // 数据操作工具对象
        Jedis j = pool.getResource();
        // 添加数据
        j.set("key", "value");
        String v = j.get("key");
        System.out.println(v);
    }
}
