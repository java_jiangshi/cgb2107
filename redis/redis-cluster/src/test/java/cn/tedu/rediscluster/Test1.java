package cn.tedu.rediscluster;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisClusterConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import java.nio.charset.StandardCharsets;

@SpringBootTest
public class Test1 {
    @Autowired
    private RedisConnectionFactory f;

    @Test
    public void test1() {
        RedisClusterConnection c = f.getClusterConnection();
        for (int i = 0; i < 100; i++) {
            String k = "k"+i;
            String v = "v"+i;
            c.set(k.getBytes(StandardCharsets.UTF_8),
                  v.getBytes(StandardCharsets.UTF_8));
        }
    }
}
